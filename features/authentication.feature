Feature: Authentication
  In order to work with gitlab.com
  I need to be able to test it

Scenario: Login to Gitlab
  Given I have access to Gitlab
  When I login with my credentials
  Then I should be logged in
