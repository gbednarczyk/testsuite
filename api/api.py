""" A module that interacts with GitlaB API"""
import os
import requests

BASE_URL = "https://gitlab.com/api/v3"

def login():
  """Logs a user into GitlaB"""
  return requests.post(BASE_URL + '/session?login=' + os.environ['GITLAB_USER'] + '&password=' + os.environ['GITLAB_PW'])
  #return response.json()

def project_list():
  """Lists all projestc for the logged in user"""
  headers = {'PRIVATE-TOKEN': os.environ['GITLAB_TOKEN']} #'a3UVtKqpPsuPyh9vbivK'
  return requests.get(BASE_URL + '/projects', headers = headers)